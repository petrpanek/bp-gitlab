package com.petrpanek.bpgitlab.util;

import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * HibernateUtil class configures Hibernate framework
 *
 * @author Petr Panek
 * @version 1.0
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    /**
     * Method registers entity objects to be saved to DB
     *
     * @return SessionFactory
     */
    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Publisher.class)
                    .addAnnotatedClass(Book.class)
                    .buildSessionFactory();
        } catch (Throwable e) {
            System.err.println("Initial SessionFactory creation failed" + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * Method closes SessionFactory
     */
    public static void shutdown() {
        getSessionFactory().close();
    }

    /**
     * Method returns SessionFactory object
     *
     * @return SessionFactory
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}
