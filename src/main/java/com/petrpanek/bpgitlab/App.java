package com.petrpanek.bpgitlab;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class of application
 *
 * @author Petr Panek
 * @version 1.0
 */
public class App extends Application {

    /**
     * Method for application start
     *
     * @param args - arguments for application
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Method for stage construction
     *
     * @param primaryStage - stage of the application
     */
    @Override
    public void start(Stage primaryStage) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/BooksView.fxml"));
            Scene scene = new Scene(root);

            primaryStage.setTitle("Book register 2000");
            primaryStage.setMaxWidth(1024);
            primaryStage.setMaxHeight(620);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
