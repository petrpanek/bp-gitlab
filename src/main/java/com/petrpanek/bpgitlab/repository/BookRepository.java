package com.petrpanek.bpgitlab.repository;

import com.petrpanek.bpgitlab.repository.entity.Book;

import java.util.List;

/**
 * Repository for managing books
 *
 * @author Petr Panek
 * @version 1.0
 */
public interface BookRepository {

    /**
     * Method saves particular book to database
     *
     * @param book - book to be saved
     */
    void saveBook(Book book);

    /**
     * Method gets book with specified id from database
     *
     * @param id - id of required book
     * @return selected book
     */
    Book getBookById(int id);

    /**
     * Method gets all books from database
     *
     * @return list of all books in database
     */
    List<Book> getAllBooks();

    /**
     * Method updates specified book in database
     *
     * @param book - book to be updated
     */
    void updateBook(Book book);

    /**
     * Method deletes a specified book
     *
     * @param book - book to be deleted
     */
    void deleteBook(Book book);

}
