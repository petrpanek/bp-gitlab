package com.petrpanek.bpgitlab.repository;

import com.petrpanek.bpgitlab.repository.entity.Publisher;

import java.util.List;

/**
 * Repository for managing publishers
 *
 * @author Petr Panek
 * @version 1.0
 */
public interface PublisherRepository {

    /**
     * Method saves particular publisher to database
     *
     * @param publisher - publisher to be saved
     */
    void savePublisher(Publisher publisher);

    /**
     * Method gets publisher with specified id from database
     *
     * @param id - id of required publisher
     * @return selected publisher
     */
    Publisher getPublisherById(int id);

    /**
     * Method gets all publishers from database
     *
     * @return list of all publishers in database
     */
    List<Publisher> getAllPublishers();

    /**
     * Methods updates specified publisher in database
     *
     * @param publisher - publisher to be updated
     */
    void updatePublisher(Publisher publisher);

    /**
     * Method deletes a specified publisher
     *
     * @param publisher - publisher to be deleted
     */
    void deletePublisher(Publisher publisher);

}
