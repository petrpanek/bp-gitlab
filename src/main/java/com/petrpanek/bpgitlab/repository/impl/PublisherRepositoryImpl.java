package com.petrpanek.bpgitlab.repository.impl;

import com.petrpanek.bpgitlab.repository.PublisherRepository;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Implementation of PublisherRepository interface
 *
 * @author Petr Panek
 * @version 1.0
 */
public class PublisherRepositoryImpl implements PublisherRepository {

    @Override
    public void savePublisher(Publisher publisher) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.save(publisher);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

    @Override
    public Publisher getPublisherById(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Publisher publisher = null;

        try {
            session.beginTransaction();
            publisher = session.createQuery("from Publisher where id = :id", Publisher.class)
                    .setParameter("id", id)
                    .getSingleResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return publisher;
    }

    @Override
    public List<Publisher> getAllPublishers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Publisher> publishers = null;

        try {
            session.beginTransaction();
            publishers = session.createQuery("from Publisher", Publisher.class).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return publishers;
    }

    @Override
    public void updatePublisher(Publisher publisher) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.update(publisher);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

    @Override
    public void deletePublisher(Publisher publisher) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.delete(publisher);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

}
