package com.petrpanek.bpgitlab.repository.impl;

import com.petrpanek.bpgitlab.repository.BookRepository;
import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

/**
 * Implementation of BookRepository interface
 *
 * @author Petr Panek
 * @version 1.0
 */
public class BookRepositoryImpl implements BookRepository {

    @Override
    public void saveBook(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.save(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

    @Override
    public Book getBookById(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Book book = null;

        try {
            session.beginTransaction();
            book = session.createQuery("from Book where id = :id", Book.class)
                    .setParameter("id", id)
                    .getSingleResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return book;
    }

    @Override
    public List<Book> getAllBooks() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Book> books = null;

        try {
            session.beginTransaction();
            books = session.createQuery("from Book", Book.class).list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

        return books;
    }

    @Override
    public void updateBook(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.update(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

    @Override
    public void deleteBook(Book book) {
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            session.beginTransaction();
            session.delete(book);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
        }

    }

}
