package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.PublisherRepository;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.PublisherRepositoryImpl;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * EditPublisherController class is a controller for EditPublisherView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class EditPublisherController extends AnchorPane implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField postNumberField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryField;

    private Publisher publisher;
    private PublisherRepository publisherRepository;

    public EditPublisherController(Publisher publisher) {

        this.publisher = publisher;
        this.publisherRepository = new PublisherRepositoryImpl();
    }

    /**
     * Method initialize data to view component
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nameField.setText(publisher.getName());
        streetField.setText(publisher.getStreet());
        postNumberField.setText(Integer.toString(publisher.getPostNumber()));
        cityField.setText(publisher.getCity());
        countryField.setText(publisher.getCountry());
    }

    /**
     * Method handles delete button click in component and deletes data from DB
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handleDeleteClicked(ActionEvent event) throws IOException {

        publisherRepository.deletePublisher(publisher);
        goTo("PublishersView", event);
    }

    /**
     * Method handles cancel button click in component and returns to PublishersView component
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handleCancelClicked(ActionEvent event) throws IOException {

        goTo("PublishersView", event);
    }

    /**
     * Method handles edit button click in component and updates data in DB
     *
     * @param event
     */
    @FXML
    public void handleEditClicked(ActionEvent event) {

        publisher.setName(nameField.getText());
        publisher.setStreet(streetField.getText());
        publisher.setPostNumber(Integer.parseInt(postNumberField.getText()));
        publisher.setCity(cityField.getText());
        publisher.setCountry(countryField.getText());

        publisherRepository.updatePublisher(publisher);
    }

    /**
     * Method handles movement between different views
     *
     * @param target location in form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent orderViewParent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene orderViewScene = new Scene(orderViewParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(orderViewScene);
        appStage.show();
    }

}
