package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.PublisherRepository;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.PublisherRepositoryImpl;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * PublisherController class is a controller for PublishersView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class PublisherController extends AnchorPane implements Initializable {

    @FXML
    private Label booksLabel;
    @FXML
    private Label publishersLabel;
    @FXML
    private TableView<Publisher> publishersTable;
    @FXML
    private TableColumn<Publisher, String> publisherNameColumn;
    @FXML
    private TableColumn<Publisher, String> publisherStreetColumn;
    @FXML
    private TableColumn<Publisher, Integer> publisherPostColumn;
    @FXML
    private TableColumn<Publisher, String> publisherCityColumn;
    @FXML
    private TableColumn<Publisher, String> publisherCountryColumn;

    private PublisherRepository publisherRepository;

    public PublisherController() {
        this.publisherRepository = new PublisherRepositoryImpl();
    }

    /**
     * Method initialize data to view component
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        publisherNameColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getName()));
        publisherStreetColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getStreet()));
        publisherPostColumn.setCellValueFactory(c -> new SimpleObjectProperty<>(c.getValue().getPostNumber()));
        publisherCityColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getCity()));
        publisherCountryColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getCountry()));

        publishersTable.getItems().setAll(publisherRepository.getAllPublishers());
    }

    /**
     * Method handles add button click and goes to AddPublisherView component
     *
     * @param event
     * @throws Exception
     */
    @FXML
    public void handleAddClicked(ActionEvent event) throws Exception {

        goTo("AddPublisherView", event);
    }

    /**
     * Method handles books label click and goes to BooksView component
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handleBooksClicked(MouseEvent event) throws IOException {

        goTo("BooksView", event);
    }

    /**
     * Method handles row click in table
     *
     * @param event
     */
    @FXML
    public void handleRowClicked(MouseEvent event) {

        Publisher clickedPublisher = publishersTable.getSelectionModel().getSelectedItem();

        if (clickedPublisher == null) {
            return;
        }

        try {
            changeScene(event, "EditPublisherView.fxml", clickedPublisher);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for handling change of scene
     *
     * @param event
     * @param sourceFile       - specifies, which view to use
     * @param clickedPublisher - clicked book from table
     * @throws Exception
     */
    private void changeScene(Event event, String sourceFile, Publisher clickedPublisher) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/" + sourceFile));
        EditPublisherController epc = new EditPublisherController(clickedPublisher);
        loader.setController(epc);

        Parent editServiceParent = loader.load();
        Scene editServiceScene = new Scene(editServiceParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(editServiceScene);
        appStage.show();
    }

    /**
     * Method handles movement between different views
     *
     * @param target location if form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene scene = new Scene(parent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
