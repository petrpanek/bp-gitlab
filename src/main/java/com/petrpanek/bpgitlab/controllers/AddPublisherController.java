package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.PublisherRepository;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.PublisherRepositoryImpl;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * AddPublisherController class is controller for AddPublisherView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class AddPublisherController extends AnchorPane {

    @FXML
    private TextField nameField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField postNumberField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField countryField;

    private PublisherRepository publisherRepository;

    public AddPublisherController() {

        this.publisherRepository = new PublisherRepositoryImpl();
    }

    /**
     * Method handles add button click in component and saves data to DB
     *
     * @param event
     */
    @FXML
    void handleAddClicked(ActionEvent event) {

        Publisher publisher = new Publisher();
        publisher.setName(nameField.getText());
        publisher.setStreet(streetField.getText());
        publisher.setPostNumber(Integer.parseInt(postNumberField.getText()));
        publisher.setCity(cityField.getText());
        publisher.setCountry(countryField.getText());

        publisherRepository.savePublisher(publisher);
    }


    /**
     * Method handles cancel button click in component and returns to PublishersView
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCancelClicked(ActionEvent event) throws IOException {

        goTo("PublishersView", event);
    }


    /**
     * Method handles movement between different views
     *
     * @param target location in form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent orderViewParent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene orderViewScene = new Scene(orderViewParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(orderViewScene);
        appStage.show();
    }

}
