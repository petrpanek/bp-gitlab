package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.BookRepository;
import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.BookRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * EditBookController class is a controller for EditBookView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class EditBookController extends GridPane implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField editionField;
    @FXML
    private TextField publishedField;
    @FXML
    private TextField isbnField;
    @FXML
    private ChoiceBox<Publisher> publisherChoiceBox;

    private Book book;
    private BookRepository bookRepository;

    public EditBookController(Book book) {

        this.book = book;
        this.bookRepository = new BookRepositoryImpl();
    }

    /**
     * Method initialize data to view component
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        nameField.setText(book.getName());
        editionField.setText(Integer.toString(book.getEdition()));
        publishedField.setText(Integer.toString(book.getYearPublished()));
        isbnField.setText(book.getIsbn());
        publisherChoiceBox.setItems(setPublishers());

    }

    /**
     * Method gets publishers from DB
     *
     * @return ObservableList - list of publishers found in DB
     */
    private ObservableList<Publisher> setPublishers() {

        return FXCollections.observableArrayList(book.getPublishers());
    }

    /**
     * Method handles delete button click in component and deletes data from DB
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handleDeleteClicked(ActionEvent event) throws IOException {

        bookRepository.deleteBook(book);
        goTo("BooksView", event);
    }

    /**
     * Method handles cancel button click in component and returns to BooksView component
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handleCancelClicked(ActionEvent event) throws IOException {

        goTo("BooksView", event);
    }

    /**
     * Method handles edit button click in component and updates data in DB
     *
     * @param event
     */
    @FXML
    public void handleEditClicked(ActionEvent event) {

        book.setName(nameField.getText());
        book.setEdition(Integer.parseInt(editionField.getText()));
        book.setYearPublished(Integer.parseInt(publishedField.getText()));
        book.setIsbn(isbnField.getText());

        bookRepository.updateBook(book);
    }

    /**
     * Method handles movement between different views
     *
     * @param target location in form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent orderViewParent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene orderViewScene = new Scene(orderViewParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(orderViewScene);
        appStage.show();
    }

}
