package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.BookRepository;
import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.impl.BookRepositoryImpl;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * BooksController class is a controller for BooksView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class BooksController extends AnchorPane implements Initializable {

    @FXML
    private Label booksLabel;
    @FXML
    private Label publishersLabel;
    @FXML
    private TableView<Book> booksTable;
    @FXML
    private TableColumn<Book, String> bookNameColumn;
    @FXML
    private TableColumn<Book, Integer> bookEditionColumn;
    @FXML
    private TableColumn<Book, Integer> bookPublishedColumn;
    @FXML
    private TableColumn<Book, String> bookISBNColumn;
    @FXML
    private TableColumn<Book, String> bookPublisherColumn;

    private BookRepository bookRepository;

    public BooksController() {

        this.bookRepository = new BookRepositoryImpl();
    }

    /**
     * Method initialize data to view component
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bookNameColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getName()));
        bookEditionColumn.setCellValueFactory(c -> new SimpleObjectProperty<>(c.getValue().getEdition()));
        bookPublishedColumn.setCellValueFactory(c -> new SimpleObjectProperty<>(c.getValue().getYearPublished()));
        bookISBNColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getIsbn()));
        bookPublisherColumn.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPublishers().toString()));

        booksTable.getItems().setAll(bookRepository.getAllBooks());
    }

    /**
     * Method for handling change of scene
     *
     * @param event
     * @param sourceFile  - specifies which view to use
     * @param clickedBook - clicked book from table
     * @throws Exception
     */
    private void changeScene(Event event, String sourceFile, Book clickedBook) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/" + sourceFile));
        EditBookController ebc = new EditBookController(clickedBook);
        loader.setController(ebc);

        Parent editServiceParent = loader.load();
        Scene editServiceScene = new Scene(editServiceParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(editServiceScene);
        appStage.show();
    }

    /**
     * Method handles add button click in component and goes to AddBookView
     *
     * @param event
     * @throws Exception
     */
    @FXML
    public void handleAddClicked(ActionEvent event) throws IOException {

        goTo("AddBookView", event);
    }

    /**
     * Method handles publishers label click and takes user to PublishersView component
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void handlePublishersClicked(MouseEvent event) throws IOException {

        goTo("PublishersView", event);
    }

    /**
     * Method handles row click in table
     *
     * @param event
     */
    @FXML
    public void handleRowClicked(MouseEvent event) {

        Book clickedBook = booksTable.getSelectionModel().getSelectedItem();

        if (clickedBook == null) {
            return;
        }

        try {
            changeScene(event, "EditBookView.fxml", clickedBook);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method handles movement between different views
     *
     * @param target location in form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene scene = new Scene(parent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

}
