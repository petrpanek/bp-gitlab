package com.petrpanek.bpgitlab.controllers;

import com.petrpanek.bpgitlab.repository.BookRepository;
import com.petrpanek.bpgitlab.repository.PublisherRepository;
import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.BookRepositoryImpl;
import com.petrpanek.bpgitlab.repository.impl.PublisherRepositoryImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * AddBookController class is controller for AddBookView component
 *
 * @author Petr Panek
 * @version 1.0
 */
public class AddBookController extends GridPane implements Initializable {

    @FXML
    private TextField nameField;
    @FXML
    private TextField isbnField;
    @FXML
    private TextField editionField;
    @FXML
    private TextField publishedYearField;
    @FXML
    private ChoiceBox<Publisher> publisherChoiceBox;

    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public AddBookController() {

        this.bookRepository = new BookRepositoryImpl();
        this.publisherRepository = new PublisherRepositoryImpl();
    }

    /**
     * Method initialize data to view component
     *
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        publisherChoiceBox.setItems(setPublishers());
    }

    /**
     * Method gets publishers from DB
     *
     * @return ObservableList - list of publishers found in DB
     */
    private ObservableList<Publisher> setPublishers() {

        return FXCollections.observableArrayList(publisherRepository.getAllPublishers());
    }


    /**
     * Method handles add button click in component and saves data to DB
     *
     * @param event
     */
    @FXML
    void handleAddClicked(ActionEvent event) {

        Book book = new Book();
        book.setName(nameField.getText());
        book.setIsbn(isbnField.getText());
        book.setEdition(Integer.parseInt(editionField.getText()));
        book.setYearPublished(Integer.parseInt(publishedYearField.getText()));
        Set<Publisher> publishers = new HashSet<>();
        publishers.add(publisherChoiceBox.getSelectionModel().getSelectedItem());
        book.setPublishers(publishers);

        bookRepository.saveBook(book);
    }


    /**
     * Method handles cancel button click in component and returns to BooksView
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCancelClicked(ActionEvent event) throws IOException {

        goTo("BooksView", event);
    }


    /**
     * Method handles movement between different views
     *
     * @param target location in form of specific view
     * @param event
     * @throws IOException
     */
    @FXML
    private void goTo(String target, Event event) throws IOException {

        Parent orderViewParent = FXMLLoader.load(getClass().getResource("/views/" + target + ".fxml"));
        Scene orderViewScene = new Scene(orderViewParent);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(orderViewScene);
        appStage.show();
    }

}
