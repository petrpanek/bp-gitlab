package com.petrpanek.bpgitlab.util;

import com.petrpanek.bpgitlab.repository.entity.Publisher;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PublisherTestDataProvider {

    public static final String NAME_PENGUIN = "Penguin Books";
    public static final String STREET_PENGUIN = "Strand";
    public static final int POST_NUMBER_PENGUIN = 80;
    public static final String CITY_PENGUIN = "London";
    public static final String COUNTRY_PENGUIN = "United Kingdom";

    public static final String NAME_ALBATROS = "Albatros";
    public static final String STREET_ALBATROS = "Na Pankraci";
    public static final int POST_NUMBER_ALBATROS = 30;
    public static final String CITY_ALBATROS = "Prague";
    public static final String COUNTRY_ALBATROS = "Czech Republic";

    public static Publisher createPublisherObject(String name, String street, int postNumber, String city, String country) {

        Publisher publisher = new Publisher();
        publisher.setName(name);
        publisher.setStreet(street);
        publisher.setPostNumber(postNumber);
        publisher.setCity(city);
        publisher.setCountry(country);

        return publisher;
    }

    public static Set<Publisher> createPublishersSet(Publisher... publishers) {

        return Stream.of(publishers).collect(Collectors.toSet());
    }
}
