package com.petrpanek.bpgitlab.util;

import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.entity.Publisher;

import java.util.Set;

public class BookTestDataProvider {

    public static final String NAME_HARRY = "Harry Potter and the Philosopher's Stone";
    public static final String ISBN_HARRY = "0-7475-3269-9";
    public static final int EDITION_HARRY = 1;
    public static final int PUBLISHED_HARRY = 1997;

    public static final String NAME_1984 = "Nineteen Eighty-Four";
    public static final String ISBN_1984 = "978-80-257-1479-9";
    public static final int EDITION_1984 = 2;
    public static final int PUBLISHED_1984 = 1949;

    public static Book createBookObject(String name, String isbn, int edition, int published, Set<Publisher> publishers) {

        Book book = new Book();
        book.setName(name);
        book.setIsbn(isbn);
        book.setEdition(edition);
        book.setYearPublished(published);
        book.setPublishers(publishers);

        return book;
    }

}
