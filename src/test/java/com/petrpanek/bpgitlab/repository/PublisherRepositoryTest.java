package com.petrpanek.bpgitlab.repository;

import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.PublisherRepositoryImpl;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static com.petrpanek.bpgitlab.util.PublisherTestDataProvider.*;

public class PublisherRepositoryTest {

    @Mock
    private PublisherRepositoryImpl publisherRepository;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPublisherById_shouldReturnPublisher_ifPublisherWithSpecifiedIdExists() {

        Publisher publisher = createPublisherObject(
                NAME_PENGUIN, STREET_PENGUIN, POST_NUMBER_PENGUIN, CITY_PENGUIN, COUNTRY_PENGUIN);
        Mockito.when(publisherRepository.getPublisherById(Mockito.anyInt())).thenReturn(publisher);

        Publisher obtainedPublisher = publisherRepository.getPublisherById(1);

        Mockito.verify(publisherRepository, Mockito.times(1)).getPublisherById(1);
        Assert.assertEquals(obtainedPublisher, publisher);
    }

    @Test
    public void getAllPublishers_shouldReturnAllPublishers_ifSomeExists() {

        Publisher publisherPenguin = createPublisherObject(
                NAME_PENGUIN, STREET_PENGUIN, POST_NUMBER_PENGUIN, CITY_PENGUIN, COUNTRY_PENGUIN);
        Publisher publisherAlbatros = createPublisherObject(
                NAME_ALBATROS, STREET_ALBATROS, POST_NUMBER_ALBATROS, CITY_ALBATROS, COUNTRY_ALBATROS);
        List<Publisher> foundPublishers = Arrays.asList(publisherPenguin, publisherAlbatros);
        Mockito.when(publisherRepository.getAllPublishers()).thenReturn(foundPublishers);

        List<Publisher> resultPublishers = publisherRepository.getAllPublishers();

        Mockito.verify(publisherRepository, Mockito.times(1)).getAllPublishers();
        MatcherAssert.assertThat(resultPublishers, Matchers.hasSize(foundPublishers.size()));
    }

}
