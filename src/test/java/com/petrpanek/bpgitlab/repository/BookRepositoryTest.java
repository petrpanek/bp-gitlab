package com.petrpanek.bpgitlab.repository;

import com.petrpanek.bpgitlab.repository.entity.Book;
import com.petrpanek.bpgitlab.repository.entity.Publisher;
import com.petrpanek.bpgitlab.repository.impl.BookRepositoryImpl;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static com.petrpanek.bpgitlab.util.BookTestDataProvider.*;
import static com.petrpanek.bpgitlab.util.PublisherTestDataProvider.*;

public class BookRepositoryTest {

    @Mock
    private BookRepositoryImpl bookRepository;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getBookById_shouldReturnBook_ifBookWithSpecifiedIdExist() {

        Publisher penguin = createPublisherObject(
                NAME_PENGUIN, STREET_PENGUIN, POST_NUMBER_PENGUIN, CITY_PENGUIN, COUNTRY_PENGUIN);
        Book harryPotter = createBookObject(
                NAME_HARRY, ISBN_HARRY, EDITION_HARRY, PUBLISHED_HARRY, createPublishersSet(penguin));
        Mockito.when(bookRepository.getBookById(Mockito.anyInt())).thenReturn(harryPotter);

        Book obtainedBook = bookRepository.getBookById(1);

        Mockito.verify(bookRepository, Mockito.times(1)).getBookById(1);
        Assert.assertEquals(obtainedBook, harryPotter);
    }

    @Test
    public void getAllBooks_shouldReturnAllBooks_ifSomeExists() {

        Publisher penguin = createPublisherObject(
                NAME_PENGUIN, STREET_PENGUIN, POST_NUMBER_PENGUIN, CITY_PENGUIN, COUNTRY_PENGUIN);
        Book harryPotter = createBookObject(
                NAME_HARRY, ISBN_HARRY, EDITION_HARRY, PUBLISHED_HARRY, createPublishersSet(penguin));
        Book orwell = createBookObject(
                NAME_1984, ISBN_1984, EDITION_1984, PUBLISHED_1984, createPublishersSet(penguin));
        List<Book> foundBooks = Arrays.asList(harryPotter, orwell);
        Mockito.when(bookRepository.getAllBooks()).thenReturn(foundBooks);

        List<Book> resultBooks = bookRepository.getAllBooks();

        Mockito.verify(bookRepository, Mockito.times(1)).getAllBooks();
        MatcherAssert.assertThat(resultBooks, Matchers.hasSize(foundBooks.size()));
    }
}
