create table book
(
    book_id        int auto_increment
        primary key,
    name           varchar(255) not null,
    isbn           varchar(255) not null,
    edition        int          not null,
    year_published int          not null
);

create table publisher
(
    publisher_id int auto_increment
        primary key,
    name         varchar(255) not null,
    street       varchar(255) not null,
    post_number  int          not null,
    city         varchar(255) not null,
    country      varchar(255) not null
);

create table book_publisher
(
    book_id      int not null,
    publisher_id int not null,
    primary key (book_id, publisher_id),
    constraint book_publisher_books_id_fk
        foreign key (book_id) references book (book_id)
            on update cascade on delete cascade,
    constraint book_publisher_publishers_id_fk
        foreign key (publisher_id) references publisher (publisher_id)
            on update cascade on delete cascade
);

